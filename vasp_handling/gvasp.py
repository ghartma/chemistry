"""
Python module for vasp files
"""

import numpy as np

# read poscar files
def readpos(filename):

    with open(filename,'r') as f:
        OTitle = f.readline()
        vfactor = f.readline()
        vfactor = float(vfactor)
        xv = f.readline()
        xv = [float(n) for n in xv.split()]
        yv = f.readline()
        yv = [float(n) for n in yv.split()]
        zv = f.readline()
        zv = [float(n) for n in zv.split()]
        OEle = f.readline()
        ev = f.readline()
        erray = [int(n) for n in ev.split()]
        dynamics = f.readline()
        coor = f.readline()
        ecount = int(np.sum(erray))
        clines = np.zeros((ecount,3))
        dlines = np.empty((ecount,3), dtype='|S1')

        for x in range(0,ecount):
            line = f.readline()
            line = line.split()
            clines[x,:] = np.array([float(line[0]),float(line[1]),float(line[2])])
            dlines[x,:] = np.array([line[3],line[4],line[5]])

    return OTitle, vfactor, xv, yv, zv, OEle, ev, erray, dynamics, coor, \
        ecount, clines, dlines

# write POSCAR file
def writepos(filename,Title,vfactor,xv,yv,zv,Ele,erray,dynamics,coor,clines,dlines):

    with open(filename,'w') as g:
        g.write("%s" % (Title))
        g.write(" %17.14f\n" % (vfactor))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (xv[0],xv[1],xv[2]))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (yv[0],yv[1],yv[2]))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (zv[0],zv[1],zv[2]))
        g.write("  %s" % (Ele))
        g.write("    ")

        for i in range(0,len(erray)):
            g.write("  %s" % (erray[i]))

        g.write("\n")
        g.write("%s" % (dynamics))
        g.write("%s" % (coor))

        for x in range(0,len(clines)):
            g.write("  %17.14f  %17.14f  %17.14f  %s  %s  %s\n" % \
                    (clines[x,0],clines[x,1],clines[x,2],dlines[x,0],
                     dlines[x,1],dlines[x,2]))

# convert direct coordinates to cartesian
def dcconv(xv,yv,zv,clines):
    a = np.linalg.norm(xv)
    b = np.linalg.norm(yv)
    c = np.linalg.norm(zv)
    alpha = np.arccos(np.dot(yv,zv)/(b*c))
    beta = np.arccos(np.dot(xv,zv)/(a*c))
    gamma = np.arccos(np.dot(xv,yv)/(a*b))
    v = np.sqrt(1 - np.cos(alpha) ** 2 - np.cos(beta) ** 2 - np.cos(gamma) ** 2 \
                + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))
    
    for x in range(0,len(clines)):
        line = np.dot([[a, b * np.cos(gamma), c * np.cos(beta)],
                       [0, b * np.sin(gamma), c * (np.cos(alpha) - np.cos(beta) * np.cos(gamma)) / np.sin(gamma)],
                       [0, 0, c * v / np.sin(gamma)]],
                      np.transpose(clines[x,:]))
        clines[x,:] = np.transpose(line)
        
    return clines

# convert cartesian coordinates to direct
def cdconv(xv,yv,zv,clines):
    a = np.linalg.norm(xv)
    b = np.linalg.norm(yv)
    c = np.linalg.norm(zv)
    alpha = np.arccos(np.dot(yv,zv)/(b*c))
    beta = np.arccos(np.dot(xv,zv)/(a*c))
    gamma = np.arccos(np.dot(xv,yv)/(a*b))
    v = np.sqrt(1 - np.cos(alpha) ** 2 - np.cos(beta) ** 2 - np.cos(gamma) ** 2 \
                + 2 * np.cos(alpha) * np.cos(beta) * np.cos(gamma))

    for x in range(0,len(clines)):
        line = np.dot([[ 1 / a, -np.cos(gamma)/( a * np.sin(gamma) ),(np.cos(alpha) * np.cos(gamma) - np.cos(beta)) / (a * v * np.sin(gamma))],
                       [0, 1 / (b * np.sin(gamma)), (np.cos(beta) * np.cos(gamma) - np.cos(alpha)) / (b * v * np.sin(gamma))],
                       [0, 0, np.sin(gamma) / (c * v)]],
                      np.transpose(clines[x,:]))
        clines[x,:] = np.transpose(line)

    return clines

# read .xyz input
def readxyz(filename):
    with open(filename,'r') as f:
        a = f.readline()
        acount = int(a)
        f.readline()
        OEle = list()
        erray = list()
        clines = np.zeros((acount,3))
        j = 0

        for i in range(0,acount):
            line = f.readline()
            line = line.split()
            clines[i,:] = np.array([float(line[1]),float(line[2]),float(line[3])])
            
            if line[0] == OEle[j]:
                erray[j] = erray[j] + 1
            else:
                j += 1
                OEle.append(line[0])
                erray.append(int(1))

        return OEle, erray, clines

# read xdatcar file
def readxdatcar(filename):
    with open(filename,'r') as f:
        OTitle = f.readline()
        vfactor = f.readline()
        vfactor = float(vfactor)

        xv = f.readline()
        xv = [float(n) for n in xv.split()]
        yv = f.readline()
        yv = [float(n) for n in yv.split()]
        zv = f.readline()
        zv = [float(n) for n in zv.split()]

        OEle = f.readline()
        ev = f.readline()
        erray = [int(n) for n in ev.split()]

        coor = f.readline()
        coor = coor.split()
        coor = coor[0]
        ecount = int(np.sum(erray))

        clines = np.zeros((ecount,3))
        clinest = np.empty((0,3))
        linen = 0

        while True:
            linen += 1
            print(linen)
            for x in range(0,ecount):
                line = f.readline()
                line = line.split()
                clines[x,:] = np.array([float(line[0]),float(line[1]),float(line[2])])
            clinest = np.concatenate((clinest,clines),axis=0)
            line = f.readline()
            if not line: break

    return OTitle, vfactor, xv, yv, zv, OEle, erray, coor, ecount, clinest, linen
