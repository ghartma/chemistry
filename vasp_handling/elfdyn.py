#!/usr/bin/env python3

"""
Analyze the electron localization functions estimated in VASP. Filters the
surface at the input isovalue to separate ELFs, calulates the centers of
these individual ELFs, calculates the shortest distance of each carbon atom
to the centers and to the isosurface. Outputs the coordinates of the ELF
centers and the distances.
"""

from sys import argv
import numpy as np
from scipy import ndimage as ndi
from scipy import spatial as spat

#input handling
def inputhandling(argv):
    if len(argv) > 2:
        filename = argv[1]
        isov = float(argv[2])
    elif len(argv) > 1:
        filename = argv[1]
        isov = 0.5
    else:
        filename = 'ELFCAR'
        isov = 0.5

    return filename,isov

#divide isosurface and locate centers
def centers(ecount,grid,vol,isov):
    labels, lcount = ndi.label(vol > isov)
    center = ndi.measurements.center_of_mass(vol, labels, range(1,lcount+1))
    center = np.array(center)
    center[:,[0,2]] = center[:,[2,0]]
    xlines = center/ grid[:,None]
    xlines = xlines.reshape((lcount,3))
    return xlines, labels

# calculate distances
def centersdistance(clines,xlines,labels, grid,xv,yv,zv):
    cdists = spat.distance.cdist(clines,xlines)
    cdists = cdists.min(axis=1)*15
    coords = clines * grid
    coords = coords.astype(int)
    edists = np.zeros(len(cdists))
    dlabels = labels < 1
    dind = ndi.distance_transform_edt(dlabels,return_distances=False,return_indices=True)
    dind = dind[:,coords[:,0],coords[:,1],coords[:,2]]
    dind = dind.transpose()
    
    for i in range(0,len(coords)):
        evec = dind[i,:] - coords[i,:]
        evec = np.divide(evec,grid)
        dvec = np.array((np.linalg.norm(xv),np.linalg.norm(yv),np.linalg.norm(zv)))
        evec = np.multiply(evec,dvec)
        edists[i] = np.linalg.norm(evec)
        
    return cdists, edists

# mains
def main():
    filename,isov = inputhandling(argv)
    Title,vfactor,xv,yv,zv,Ele,ev,erray,coor,ecount,clines = readpos(filename)
    grid,vol = readvol(filename,ecount)
    xlines,labels = centers(ecount,grid,vol,isov)
    cdists,edists = centersdistance(clines,xlines,labels,grid,xv,yv,zv)
    writepos(filename+'.out','elfcenters',vfactor,xv,yv,zv,Ele,erray,'Selective dynamics',coor,clines,xlines)
    writedists(filename+'.txt',cdists,edists)


# read atomic coordinates from ELFCAR, differs from POSCAR format
def readpos(filename):
        with open(filename,'r') as f:
            OTitle = f.readline()
            vfactor = f.readline()
            vfactor = float(vfactor)
            xv = f.readline()
            xv = [float(n) for n in xv.split()]
            yv = f.readline()
            yv = [float(n) for n in yv.split()]
            zv = f.readline()
            zv = [float(n) for n in zv.split()]
            OEle = f.readline()
            ev = f.readline()
            erray = [int(n) for n in ev.split()]
            coor = f.readline()
            ecount = int(np.sum(erray))
            clines = np.zeros((ecount,3))
            dlines = np.empty((ecount,3), dtype='|S1')
            for x in range(0,ecount):
                line = f.readline()
                line = line.split()
                clines[x,:] = np.array([float(line[0]),float(line[1]),float(line[2])])
        return OTitle, vfactor, xv, yv, zv, OEle, ev, erray, coor, \
        ecount, clines

# read volumetric data from ELFCAR
def readvol(filename,ecount):
    grid = np.loadtxt(filename,dtype=int,skiprows=(ecount+9),max_rows=1)
    mcount = np.prod(grid)
    mlines = int(np.ceil(mcount/10))
    vola = np.loadtxt(filename,skiprows=(ecount+10),max_rows=mlines)
    volb = np.loadtxt(filename,skiprows=(ecount+11+mlines),max_rows=mlines)
    vol = np.add(vola,volb)/2
    vol = vol.flatten()
    vol = vol.reshape((np.flip(grid)))
    grid = np.array(grid).reshape((1,3))
    
    return grid, vol

# write positions of atomic coordintates with centers of separated ELFs in POSCAR format
def writepos(filename,Title,vfactor,xv,yv,zv,Ele,erray,dynamics,coor,clines,xlines):
    with open(filename,'w') as g:
        g.write("%s\n" % (Title))
        g.write(" %17.14f\n" % (vfactor))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (xv[0],xv[1],xv[2]))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (yv[0],yv[1],yv[2]))
        g.write("  %17.14f  %17.14f  %17.14f\n" % (zv[0],zv[1],zv[2]))
        g.write("  %s  " % (Ele))
        g.write("    ")
        
        for i in range(0,len(erray)):
            g.write("  %s" % (erray[i]))
            
        g.write(" %s\n" % (len(xlines)))
        g.write("%s\n" % (dynamics))
        g.write("%s" % (coor))
        
        for x in range(0,len(clines)):
            g.write("  %17.14f  %17.14f  %17.14f \n" % \
                        (clines[x,0],clines[x,1],clines[x,2]))
            
        for x in range(0,len(xlines)):
            g.write("  %17.14f  %17.14f  %17.14f \n" % \
                        (xlines[x,0],xlines[x,1],xlines[x,2]))

# write per-atom distances from ELF centers and distances to isosurface
def writedists(filename,cdists,edists):
    with open(filename, 'w') as g:
        g.write("atom   cdist    edist\n")
        
        for i in range(0,len(cdists)):
            g.write(" %3i    %6.3f   %6.3f\n" % (i+1, cdists[i], edists[i]))

if __name__ == "__main__":
    main()
