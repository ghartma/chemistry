#!/usr/bin/env python3
from sys import argv
import numpy as np
from scipy import spatial as spat
import pdb

filename = argv[1]

def centersdistance(clines,xlines):
    cdists = spat.distance.cdist(clines,xlines)
    cdists = cdists.min(axis=1)
    return cdists

def main():
    clines,xlines = readpos(filename)
    cdists = centersdistance(clines,xlines)
    writedists(filename+'.txt',cdists)

def readpos(filename):
    with open(filename, 'r') as f:
        count = f.readline()
        count = int(count)
        f.readline()
        clines = []
        xlines = []
        for i in range(0,count):
            line = f.readline()
            line = line.split()
            if line[0] != 'X':
                clines.append([float(line[1]),float(line[2]),float(line[3])])
            else:
                xlines.append([float(line[1]),float(line[2]),float(line[3])])
        return clines, xlines

def writedists(filename,cdists):
    with open(filename, 'w') as g:
        g.write("atom   cdist\n")
        for i in range(0,len(cdists)):
            g.write(" %3i    %6.3f\n" % (i+1, cdists[i]))

if __name__ == "__main__":
    main()
