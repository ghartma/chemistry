#!/usr/bin/env python3
"""
Average the local potential across the surface at each atom, providing
the average electrode potential, Vs. Reads in any potential profile.
"""
#declarations
filename = 'LOCPOT'

#calculate average
def calculate(locpot):
    avg = sum(locpot) / float(len(locpot)) # int->float may be redundant in python3
    return avg

#mains
def main(filename):
    locpot = readinput(filename)
    avg = calculate(locpot)
    print(avg)

#read input
def readinput(filename):
    with open(filename,'r') as f:
        for i in range(0,6):
            f.readline()

        line = f.readline()
        line1 = line.split()
        line2 = [int(i) for i in line1]
        natom = sum(line2)

        for i in range(0,natom+2):
            f.readline()
    
        line = f.readline()
        line1 = line.split()
        NGX = int(line1[0])
        NGY = int(line1[1])
        NGZ = int(line1[2])
        NPLWV = NGX*NGY*NGZ
        locpot = []
    
        for i in range(0,NPLWV/5):
            line = f.readline()
            line1 = line.split()
            locpot.extend(line1)

    locpot = [float(i) for i in locpot]
    return locpot
    
if __name__ == "__main__":
    main(filename)
