#!/usr/bin/env python3
"""
Average the local potential profiles across the normal vector for each 
atom, providing the average profile used to determine the potential 
drop to the solvent. Reads in LOCPOT.
"""
import numpy as np
import scipy.spatial as sps
import gvasp

#declarations
filename = 'LOCPOT'

#calculate the average of profiles across each surface normal
def calculate(locpot,cpoints,NGZ,natom,pointsb,NGX,NGY):        
    locpot = [float(i) for i in locpot]
    tree = sps.KDTree(cpoints, leafsize = 10)
    sumprofile = [0.0 for i in range(0,NGZ)]
    
    for i in range(0,natom):
        profile = []
        center = int(pointsb[i][2] * NGZ) * NXY + int(pointsb[i][1] * NGY) * NGX + int(pointsb[i][0] * NGX)
        d, i = tree.query(cpoints[i,:], k=6 )
        p1 = cpoints[i[1],:]
        p2 = cpoints[i[2],:]
        p3 = cpoints[i[3],:]
        p4 = cpoints[i[0],:]
        v14 = p1-p4 
        v24 = p2-p4
        v34 = p3-p4
        v21 = p2-p1
        v31 = p3-p1
        v41 = p4-p1
        v13 = p1-p3
        v12 = p1-p2
        v42 = p4-p2
        v43 = p4-p3
        normaln = np.cross(v21,v31)
        normall = np.linalg.norm(normaln)
        normal = normaln/ normall

        if normal[2] < 0:
            normal = normal * -1.0

        if (np.abs(np.linalg.norm(v34)) < 2):
            for j in range(-NGZ/2,NGZ/2):
                k = center + int(normal[0] * j) + int(normal[1] * j ) * NGX + int(normal[2] * j) * NXY
                if k < 0: k = k + NPLWV
                if k > NPLWV: k = k - NPLWV
                profile.append(locpot[k])

            sumprofile = [sumprofile[i] + profile[i] for i in range(0,NGZ)]
        else: natom -= 1
    aveprofile = [sumprofile[i] / natom for i in range(0,NGZ)]
    return aveprofile

#read input file
def readinput(filename):
    with open(filename,'r') as f:
        for i in range(0,2):
            f.readline()
            
        xv = f.readline()
        xv = [float(n) for n in xv.split()]
        yv = f.readline()
        yv = [float(n) for n in yv.split()]
        zv = f.readline()
        zv = [float(n) for n in zv.split()]
        z = zv[2]
        line = f.readline()
        ele = line.split()
        line = f.readline()
        line1 = line.split()
        atom0 = [int(i) for i in line1]
        natom = 0
        xi = []
        yi = []
        zi = []
        f.readline()
        points = np.zeros((sum(atom0),3))
    
        for i in range(0,len(ele)):
            if ele[i] == 'C':
                for j in range(0,atom0[i]):
                    line = f.readline()
                    line = line.split()
                    points[natom+j,:] = np.array([float(line[0]),float(line[1]),float(line[2])])
                natom = natom + atom0[i]
            else:
                for j in range(0,atom0[i]):
                    f.readline()
    
        pointsb = points.tolist()
        cpoints = gvasp.dcconv(xv,yv,zv,points)
        f.readline()
        line = f.readline()
        line1 = line.split()
        NGX = int(line1[0])
        NGY = int(line1[1])
        NGZ = int(line1[2])
        NPLWV = NGX*NGY*NGZ
        NXY = NGX*NGY
        locpot = []
        zs = z / float(NGZ)

        for i in range(0,NPLWV/5):
            line = f.readline()
            line1 = line.split()
            locpot.extend(line1)
        return NGX,NGY,NGZ,natom,zs,locpot,cpoints

#mains    
def main(filename):
    NGX,NGY,NGZ,natom,zs,locpot,cpoints,pointsb = readinput(filename)
    aveprofile = calculate(locpot,cpoints,NGZ,natom,pointsb,NGX,NGY)
    writeoutput(NGZ,zs,aveprofile)

#write output file    
def writeoutput(NGZ,zs,aveprofile):
    with open("LOCPOT_Zn",'w') as g:
        for i in range(0,NGZ):
            z = zs * (float(i) + 0.5)
            g.write(" %f %f\n" % (z,aveprofile[i]))

if __name__ == "__main__":
    main(filename)



    
