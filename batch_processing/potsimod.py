#!/usr/bin/env python3
"""
Average the local potential across the surface at each atom, providing
the average electrode potential, Vs. Reads in any potential profile.
"""
from sys import argv

#declarations
tol = 0.5
width = 1.482 #heuristic thickness of basal plane
filename = argv[1]

# calculate surface average
def calculate(tol,width,pot):
    lim = pot[i] + tol
    j = i
    potsum = pot[i]

    while abs(j-i) < width:
        j -= 1
        potsum = potsum + pot[j]
        nav += 1

    j = i
    while abs(j-i) < width:
        j += 1
        potsum = potsum + pot[j]
        nav += 1
    
    pots = potsum / nav
    return pots

#mains
def main(filename,tol,width):
    pot = readinput(filename)
    pots = calculate(tol,width,pot)
    print(pots)

# read input file
def readinput(filename):
    z = []
    pot = []
    nz = 0
    nav = 1

    with open(filename,'r') as f:
        line = f.readline()
        while True:
            line = f.readline()
            if not line: break
            line = line.split()
            z.append(float(line[0]))
            pot.append(float(line[1]))
            nz += 1

    i = nz / 2
    width = int(nz * (width/25.0))
    return pot
    
if __name__ == "__main__":
    main(filename,tol,width)
