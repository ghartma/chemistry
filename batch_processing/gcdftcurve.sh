#!/bin/sh
# [Greg Hartmann] GCDFT Curve
# sh script to automate the post-processing of GC-DFT datasets
# compatible with python3
# using the legacy code vtotav.py requires ASE

#loop over completed data within ./<folders named by NELECT>
i=1
for dir in */
do
    if [ -d $dir/acc/ ]; then
	echo $i
	dp=`basename "$dir"`
	sd=`dirname "$0"`

	cd $dir/acc/
	if [ ! -f LOCPOT_Zn ]; then
	    # using surface-normal profiles in [waiting on review]
	    python $sd/normprof.py
	    #context for the legacy code available on vasp forums
	    #vtotav.py LOCPOT z > /dev/null 
	fi
	avg=$(python $sd/locpotavg.py)
	cd ../..
	
	e0=$(grep sigma $dir/acc/OUTCAR | awk 'END{print $4}')
	ferm=$(grep fermi $dir/acc/OUTCAR | awk 'END{print $3}')
	efs=$(grep FERMI_SHIFT $dir/acc/out.log | awk 'END{print $3}')
	vs=$(python $sd/potsimod.py $dir/acc/LOCPOT_Zn)	
	#context for the legacy code
	#vs=$(python $sd/potsimod.py $dir/acc/LOCPOT)
	vinf=$(awk 'END{print $2}' $dir/acc/LOCPOT_Zn)

	#print data
	echo "$dp   $e0   $ferm    $efs    $vs    $vinf    $avg" >> output.txt
	((++i))
    fi
done

#print header
sed -i '1s/^/n   e0   Ef   Efshit       Vs        Vinf       Vavg   \n/' output.txt

#terminate
exit 0
