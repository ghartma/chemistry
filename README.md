# Chemistry Scripts and Programs

A portfolio of scripts and programs for post-processing data from first-principles simulations.

### Batch processing

Scripts used to generate *ΔG* profiles from a series of VASP calculations varying *q* (GC-DFT).

### Metadynamics postprocessing

Light codes to generate the free energy surface from metadynamics runs within VASP, CPMD, or CP2K. Necessary to provide output in the format used in other scripts or where a utility isn't provided.

### VASP handling

Scripts to manipulate POSCAR formats and analyze the result of VASP simluations.

### VMD scripting

Scripts used within VMD to analyze trajectories.

## Author

**Gregory Hartmann**

- [Profile](https://bitbucket.org/ghartma)
- [Email](mailto:ghartma@utexas.edu)
- [Website](https://greghartmann.info)

