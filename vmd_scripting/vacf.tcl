#modified script to provide velocity autocorrelation function
#and msd
########################################################################
# msd correlation length in frames
set msdlen 10
# time delta between frames in ps
set delt 0.1
# output file
set outfile "testmsd.dat"
# atom selection
set sel [atomselect top "type 5"]
#set sel [atomselect top "index 96 or index 97"]
#set sel [atomselect top "index 153 or index 152"]
########################################################################
# for numerical derivatives
package require sgsmooth 1.1
package require specden

set nf [molinfo top get numframes]
set idxlist [$sel get index]
$sel delete

# number of msd data sets
set nummsd 0
# initialize msd array
for {set i 0} {$i < $msdlen} {incr i} {
    set msd($i) 0.0
    set msdxy($i) 0.0
}

# loop over residues
foreach idx $idxlist {
    puts "processing index $idx"
    set poslist {}
    set sel [atomselect top "index $idx"]

    # fill poslist
    for {set i 0} {$i < $msdlen} {incr i} {
        $sel frame $i
        lappend poslist [measure center $sel]
    }

    # compute msd for the rest of the trajectory
    set nf [molinfo top get numframes]
    for {set i $msdlen} {$i < $nf} {incr i} {

        set ref [lindex $poslist 0]
	set xyref [lrange $ref 0 1]
        set poslist [lrange $poslist 1 end]
        $sel frame $i
        lappend poslist [measure center $sel]
	
        set j 0
        foreach pos $poslist {
	    set msd($j) [expr $msd($j) + [veclength2 [vecsub $ref $pos]]]
	    set msdxy($j) [expr $msdxy($j) + [veclength2 [vecsub [lrange $pos 0 1] $xyref]]]
            incr j
	}
        incr nummsd
    }
    
    $sel delete
}

set tval {}
set msdval {}
set msdxyval {}

# normalize and build lists for output
for {set i 0} {$i < $msdlen} {incr i} {
    lappend tval [expr $delt * ($i +1)]
    lappend msdval [expr $msd($i)/$nummsd]
    lappend msdxyval [expr $msdxy($i)/$nummsd]
}

# compute numerical derivative
set msdder [sgsderiv $msdval 2 4]
set msdxyder [sgsderiv $msdxyval 2 4]

# write out
set fp [open $outfile w]
foreach t $tval m $msdval d $msdder mxy $msdxyval dxy $msdxyder {
    puts $fp "$t  $m  [expr {$d/6.0}]  $mxy  [expr {$dxy/4.0}]"
}
close $fp
