#determines the hydrogen bonding auto correlation function
# input [lower limit] [upper limit] [lagtime] [outfile]
#HB O-O distance limits, should default to 5,8
#lag time defaults to 10

########################################################################
set cutoff 2.5
#acf length in fs
set acflen 100
set delt 250
########################################################################
#read input arguments
set llimit [lindex $argv 0]
set ulimit [lindex $argv 1]
set k [lindex $argv 2]
set outfile [lindex $argv 3]
########################################################################
set nf [molinfo top get numframes]
set nf [expr $nf - $acflen]
set sel [atomselect top "all"]

# number of windows
set pcount 0
set nwm 0

# intialize pacf arra
for {set i 0} {$i < $acflen} {incr i} {
    set acf($i) 0
}

# time loop
for {set i 0} {$i < $nf} {incr i $k} {
    if {$pcount == 500} {
	puts $i
	set pcount 0
    }
    #atom loop
    set tsel [atomselect top "element O and z > $llimit and z < $ulimit" frame $i]
    set idxlist [$tsel get index]
    set nw [$tsel num]
    foreach idx $idxlist {
	set hsel [atomselect top "element H and not same fragment as index $idx and within $cutoff of index $idx" frame $i]
	set hdxlist [$hsel get index]
	#window loop
	foreach hdx $hdxlist {
	    for {set j 0} {$j < $acflen} {incr j} {
		set m [expr $i + $j]
		$tsel frame $m
		$hsel frame $m
		set Ac [measure center [atomselect top "index $idx"]]
		set Hc [measure center [atomselect top "index $hdx"]]
		set distv [vecsub $Ac $Hc]
		set dist [veclength $distv]
		if {$dist > $cutoff} {
		    break
		} else {
		    incr acf($j)
		}
	    }
	}
    }
    incr pcount $k
}

set tval {}
set acfval {}

#normalize and build list for output
for {set i 0} {$i < $acflen} {incr i} {
    lappend tval [expr $delt * ($i)]
    lappend acfval [expr double($acf($i)) / double($acf(0))]
}

# write out
set fp [open $outfile w]
foreach t $tval p $acfval {
    puts $fp "$t  $p"
}
close $fp

