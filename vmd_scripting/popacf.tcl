#deremines the H2O layer popluation auto correlation function
# input [lower limit] [upper limit] [lagtime] [delta] [outfile]
# defaults 5,8,100,250

########################################################################
#acf length in fs
set acflen 1000
########################################################################
#read input arguments
set llimit [lindex $argv 0]
set ulimit [lindex $argv 1]
set k [lindex $argv 2]
set delt [lindex $argv 3]
set outfile [lindex $argv 4]
########################################################################
set nf [molinfo top get numframes]
set nf [expr $nf - $acflen]
set sel [atomselect top "all"]

# number of windows
set pcount 0
set nwm 0

# intialize pacf arra
for {set i 0} {$i < $acflen} {incr i} {
    set acf($i) 0
}

# time loop
for {set i 0} {$i < $nf} {incr i $k} {
    if {$pcount == 500} {
	puts $i
	set pcount 0
    }
    #atom loop
    $sel frame $i
    set tsel [atomselect top "type 2 and z > $llimit and z < $ulimit" frame $i]
#    set tsel [atomselect top "element O and z > $llimit and z < $ulimit" frame $i]
    set idxlist [$tsel get index]
    set nw [$tsel num]
    foreach idx $idxlist {
	#window loop
	for {set j 0} {$j < $acflen} {incr j} {
	    set m [expr $i + $j]
	    set asel [atomselect top "index $idx" frame $m]
	    set z [$asel get z]
	    if {$z < $llimit || $z > $ulimit} {
		break
	    } else {
		incr acf($j)
	    }
	}
    }
    incr nwm $nw
    incr pcount $k
}

set tval {}
set acfval {}

#normalize and build list for output
for {set i 0} {$i < $acflen} {incr i} {
    lappend tval [expr $delt * ($i)]
    lappend acfval [expr double($acf($i)) / double($nwm)]
}

# write out
set fp [open $outfile w]
foreach t $tval p $acfval {
    puts $fp "$t  $p"
}
close $fp

