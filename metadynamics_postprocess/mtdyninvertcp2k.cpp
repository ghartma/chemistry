/* calculate the free energy surface resulting from a CP2K
   run with a single distance collective variable

   input: CP2K formatted hills file
   output: profile.txt
   
   Greg Hartmann 08/28/2018 
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <math.h>
#include <algorithm>
#include <vector>
#include <iomanip>

using namespace std;

// gaussian function
double gauss(double x, double mu, double gh, double w) {
  double y;
  y = (gh * exp (-0.5 * pow(( (x - mu) / w ), 2)));
  return y;
}

// sum of gaussians
double gauss_sum(double x, vector<double> X, vector<double> gh, vector<double> gw) {
  double y;
  int gaussians = gh.size();
  for (int i = 0; i < gaussians; i++)
    {
      y = y + gauss(x, X[i], gh[i], gw[i]) ;
    }
  return y;
}

// build profile
vector<double> build(int npoints, vector<double> X, vector<double> gh, vector<double> gw) {
  vector<double> x;
  double x2;
  double y;
  double ymax = 0.0;
  double mi = *min_element(X.begin(),X.end()) - 1;
  double ma = *max_element(X.begin(),X.end()) + 1;
  double d = (ma - mi) / npoints;
  vector<double> profile;
  
  for (int i = 0; i < npoints; i++)
    {
      x2 = mi + (i + 0.5) * d;
      x.push_back(x2); 
      y = gauss_sum(x2, X, gh, gw);
      profile.push_back(y);
      if (y > ymax)
	{
	  ymax = y;
	}
    }
  for (int i = 0; i < npoints; i++)
    {
      profile[i] = ymax - profile[i];
    }
  return profile;
}

// read data file
void input(string p_arg, vector<double> &X, vector<double> &gh, vector<double> &gw) {
  const double convd = 0.52917720859; // Bohr to angstrom
  const double conve = 27.21138386; // Ha to eV
  ifstream inc;
  string linec;
  ifstream inp;
  string linep;
  istringstream ssp;
  double ghi;
  double gwi;
  double xi;
  double dummy;

  inp.open(p_arg.c_str());
 
  if (inp.is_open()) {
    while (getline(inp,linep)) {
      ssp.str(linep);
      ssp >> dummy >> xi >> gwi >> ghi;
      X.push_back(xi * convd);
      gh.push_back(ghi * conve);
      gw.push_back(gwi * convd);
    }
    inp.close();
  }
  else {
    cout << "Error opening hills file" << p_arg << endl;
  }
}

// output
void output(int npoints, vector<double> x, vector<double> profile) {
  ofstream gfile;
  gfile.open("profile.txt");
  gfile << fixed << setprecision(6);
  
  for (int i = 0; i < npoints; i++)
    {
      gfile << x[i] << " " << profile[i] << endl;
    }
  gfile.close();
}

// mains
int main(int argc, char* argv[]) { 
  string p_arg = argv[0];
  vector<double> X;
  vector<double> gh;
  vector<double> gw;
  vector<double> x;
  vector<double> profile;
  int npoints = 1000;

  input(p_arg, X, gh, gw);
  profile = build(npoints, X, gh, gw);
  output(npoints, x, profile);
  return 0;
}
